<?php

namespace CupomaticoReports;

use CupomaticoReports\Request\HttpClient;

abstract class AbstractHandler
{
    /**
     * The HTTP Client
     *
     * @var HttpClient
     */
    protected $httpClient;

    public function __construct($httpClient)
    {
        $this->httpClient = $httpClient;
    }
}
