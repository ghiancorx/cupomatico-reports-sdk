<?php

namespace CupomaticoReports\GalaxisReports;

use CupomaticoReports\GalaxisReports\Request\BetAndDurationSessionFetch;
use CupomaticoReports\AbstractHandler;

class GalaxisReportsHandler extends AbstractHandler
{
    public function fetchBetAndDurationSession($criteria)
    {
        $request = new BetAndDurationSessionFetch($criteria);
        $response = $this->httpClient->send($request);
        return $response->data;
    }
}
