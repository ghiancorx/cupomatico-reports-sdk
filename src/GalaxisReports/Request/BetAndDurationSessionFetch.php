<?php

namespace CupomaticoReports\GalaxisReports\Request;

use CupomaticoReports\Exception\InvalidCriteriaGalaxisException;
use CupomaticoReports\RequestInterface;

class BetAndDurationSessionFetch implements RequestInterface
{
    private $hasException = false;
    private $fieldsException = array();
    private $criteria;

    public function __construct($criteria)
    {
        $this->criteria = $criteria;
        if (!isset($this->criteria->startDate)){
            $this->hasException = true;
            $this->fieldsException[] = 'startDate';
        }
        if (!isset($this->criteria->endDate)){
            $this->hasException = true;
            $this->fieldsException[] = 'endDate';
        } 
        if ($this->hasException) {
            throw new InvalidCriteriaGalaxisException('El/los parametros ' . implode(', ', $this->fieldsException) . ' son requeridos.', 500);
        }
    }

    public function getCriteria()
    {
        return $this->criteria;
    }

    public function getStartDate(){
        return  $this->getCriteria()->startDate;
    }

    public function getEndDate(){
        return  $this->getCriteria()->endDate;
    }

    public function getPath()
    {
        return sprintf("%s/customer/betAndDurationSession/%s/%s", $this->getApiUrl(), $this->getStartDate(), $this->getEndDate());
    }

    public function getPayload()
    {
        return [];
    }

    public function getMethod()
    {
        return self::HTTP_GET;
    }

    public function getApiUrl()
    {
        return self::REQUEST_URI_GALAXIS;
    }
}
