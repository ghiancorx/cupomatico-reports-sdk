<?php

namespace CupomaticoReports;

use CupomaticoReports\Exception\InvalidRequestException;
use CupomaticoReports\RequestInterface;
use GuzzleHttp\Client;

class RestClient
{

    private $token;
    /**
    * @var Client
    */
    private $client;

    public function __construct($token)
    {
        $this->token = $token;
        $this->client = new Client();
    }

    public function send(RequestInterface $request)
    {
        return $this->execute($request->getPath(), $request->getMethod(), $request->getPayLoad());
    }

    private function execute($url, $method, $body)
    {
        $headers = array(
            "Authorization" => 'Bearer '. $this->getToken(),
            "Accept" => 'application/json',
            "Content-Type" => 'application/json'
        );

        $options = array('headers' => $headers);

        if ($method == "POST") {
            $options = array_merge($options, ['body' => $body]);
            $response = $this->client->post($url, $options);
        } else {
            $response = $this->client->get($url, $options);
        }

        $statusCode = $response->getStatusCode();

        if ($statusCode != 200) {
            if ($statusCode == 404) {
                throw new InvalidRequestException('El recurso solicitado no existe');
            } elseif($statusCode >= 400 && $statusCode < 500) {
                $responseContent = json_decode($response->getBody()->getContents());
                throw new InvalidRequestException($responseContent->status);
            } else {
                throw new InvalidRequestException('El recurso solicitado tiene un error interno, contacta con el desarrollador');
            }
        }

        return json_decode($response->getBody()->getContents());
    }

    public function getToken()
    {
        return $this->token;
    }
}
