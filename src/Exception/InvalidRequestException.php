<?php

namespace CupomaticoReports\Exception;

class InvalidRequestException extends CupomaticoReportsException
{
}
