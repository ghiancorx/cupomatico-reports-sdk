<?php

namespace CupomaticoReports\Exception;

class InvalidCriteriaCupomaticoException extends CupomaticoReportsException
{
}
