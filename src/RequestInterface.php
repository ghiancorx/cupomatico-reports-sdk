<?php

namespace CupomaticoReports;

interface RequestInterface
{
    const HTTP_GET = "GET";
    const HTTP_POST = "POST";

    const REQUEST_URI_GALAXIS = "http://localhost/cupomatico-reports/api/galaxis";
    const REQUEST_URI_CUPOMATICO = "http://localhost/cupomatico-reports/api/cupomatico";

    public function getPayload();

    public function getPath();

    public function getMethod();

    public function getApiUrl();

}
