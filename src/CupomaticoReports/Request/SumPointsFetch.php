<?php

namespace CupomaticoReports\CupomaticoReports\Request;

use CupomaticoReports\Exception\InvalidCriteriaCupomaticoException;
use CupomaticoReports\RequestInterface;

class SumPointsFetch implements RequestInterface
{
    private $hasException = false;
    private $fieldsException = array();
    private $criteria;

    public function __construct($criteria)
    {
        $this->criteria = $criteria;
        if (!isset($this->criteria->startDate)){
            $this->hasException = true;
            $this->fieldsException[] = 'startDate';
        }
        if (!isset($this->criteria->endDate)){
            $this->hasException = true;
            $this->fieldsException[] = 'endDate';
        } 
        if ($this->hasException) {
            throw new InvalidCriteriaCupomaticoException('El/los parametros ' . implode(', ', $this->fieldsException) . ' son requeridos.', 500);
        }
    }

    public function getCriteria()
    {
        return $this->criteria;
    }

    public function getPath()
    {
        $startDate = $this->getCriteria()->startDate;
        $endDate = $this->getCriteria()->endDate;
        return sprintf("%s/customer/sumPoints/%s/%s", $this->getApiUrl(), $startDate, $endDate);
    }

    public function getPayload()
    {
        return [];
    }

    public function getMethod()
    {
        return self::HTTP_GET;
    }

    public function getApiUrl()
    {
        return self::REQUEST_URI_CUPOMATICO;
    }
}
