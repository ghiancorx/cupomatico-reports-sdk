<?php

namespace CupomaticoReports\CupomaticoReports\Request;

use CupomaticoReports\RequestInterface;

class EnabledGiftshopPoints implements RequestInterface
{
    private $year;

    public function __construct($year = null)
    {
        $this->year = $year;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getPath()
    {
        $queryParams = '';
        if (is_numeric($this->getYear())) {
            $queryParams = '?year=' . $this->getYear();
        }
        return sprintf("%s/customer/enabledGiftshopPoints%s", $this->getApiUrl(), $queryParams);
    }

    public function getPayload()
    {
        return [];
    }

    public function getMethod()
    {
        return self::HTTP_GET;
    }

    public function getApiUrl()
    {
        return self::REQUEST_URI_CUPOMATICO;
    }
}
