<?php

namespace CupomaticoReports\CupomaticoReports;

use CupomaticoReports\CupomaticoReports\Request\SumPointsFetch;
use CupomaticoReports\AbstractHandler;
use CupomaticoReports\CupomaticoReports\Request\EnabledGiftshopPoints;

class CupomaticoReportsHandler extends AbstractHandler
{
    public function fetchSumPoints($criteria)
    {
        $request = new SumPointsFetch($criteria);
        $response = $this->httpClient->send($request);
        return $response->data;
    }

    public function fetchEnabledGiftshopPoints($year = null)
    {
        $request = new EnabledGiftshopPoints($year);
        $response = $this->httpClient->send($request);
        return $response->data;
    }
}
