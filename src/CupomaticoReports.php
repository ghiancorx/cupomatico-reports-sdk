<?php

namespace CupomaticoReports;

use CupomaticoReports\GalaxisReports\GalaxisReportsHandler;
use CupomaticoReports\CupomaticoReports\CupomaticoReportsHandler;
use CupomaticoReports\RestClient;

class CupomaticoReports
{
    /**
     * @var RestClient
     */
    protected $restClient;

    /**
     * The galaxis reports handler
     *
     * @var GalaxisReportsHandler
     */
    private $galaxisReportsHandler;

    /**
     * The cupomatico reports handler
     *
     * @var CupomaticoReportsHandler
     */
    private $cupomaticoReportsHandler;

    public function __construct($token)
    {
        $this->restClient = new RestClient($token);
    }

    /**
     * Gets the galaxis reports handler
     *
     * @return GalaxisReportsHandler
     */
    public function galaxisReports()
    {
        if (!$this->galaxisReportsHandler instanceof GalaxisReportsHandler) {
            $this->galaxisReportsHandler = new GalaxisReportsHandler($this->restClient);
        }
        return $this->galaxisReportsHandler;
    }

    /**
     * Gets the cupomatico reports handler
     *
     * @return CupomaticoReportsHandler
     */
    public function cupomaticoReports()
    {
        if (!$this->cupomaticoReportsHandler instanceof CupomaticoReportsHandler) {
            $this->cupomaticoReportsHandler = new CupomaticoReportsHandler($this->restClient);
        }
        return $this->cupomaticoReportsHandler;
    }
}