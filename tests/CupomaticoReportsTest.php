<?php

namespace CupomaticoReports\Tests;

use CupomaticoReports\CupomaticoReports;
use PHPUnit\Framework\TestCase;
use stdClass;

class CupomaticoReportsTest extends TestCase
{
    /**
     * @test
     */
    public function itFetchReports()
    {
        // use CupomaticoReports\CupomaticoReports;
        // require 'vendor/autoload.php';
        
        $cupomaticoReports = new CupomaticoReports('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJGQzUzMTAxOC1ERUI0LTQ4NzgtQkQ4Ni0zQjA0MUZENUQ1NDQiLCJlbWFpbCI6InNhMTY1Njk1MDgwOUBtYXNhcmlzLmNvbSIsIm5hbWUiOiJzYSIsImNvbm5lY3Rpb25JcCI6IjEwLjQ1LjQuNzAiLCJzZXJ2ZXJBZGRyZXNzSXAiOiIxMC40NS40LjMwIiwicm9vbWdhbWVJZCI6IjdGRkQyQjk0LTg3N0MtRTUxMS04ODFDLTAwNTA1NkE2MjdBRSIsImlhdCI6MTY1Njk1MTczNiwiZXhwIjoxNjU5NTQzNzM2fQ.Qj1ZAe4hxe6UlQktQ4mnd4IgpigCIthdLAvG1OpFZZM');
        $criteria = new stdClass();
        $criteria->startDate = '2022-01-01';
        $criteria->endDate = '2022-06-01';
        var_dump($cupomaticoReports->galaxisReports()->fetchBetAndDurationSession($criteria));
    }
}